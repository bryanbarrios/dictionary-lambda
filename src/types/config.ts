export type APIConfig = {
  apiURL: string;
  apiVersion: string;
};

export type AppConfig = {
  api: APIConfig;
};
