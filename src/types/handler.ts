import type { Handler as LambdaHandler } from "aws-lambda";

import type { ExceptionBase } from "@/lib/exceptions/exception-base";

export type HandlerVariables<TVariables extends Record<string, unknown>> = {
  variables: TVariables;
};

export type Handler<
  TVariables extends Record<string, unknown>,
  TResult extends object,
  TError extends ExceptionBase
> = LambdaHandler<HandlerVariables<TVariables>, TResult | TError>;
