export type Entries = Entry[];

export type Entry = {
  word?: string;
  phonetic?: string;
  phonetics?: Phonetic[];
  origin?: string;
  meanings?: Meaning[];
};

export type Meaning = {
  partOfSpeech?: string;
  definitions?: Definition[];
};

export type Definition = {
  definition?: string;
  example?: string;
  synonyms?: string[];
  antonyms?: string[];
};

export type Phonetic = {
  text?: string;
  audio?: string;
};

export enum DictionaryExceptionCode {
  WordNotFound = "WORD.NOT_FOUND",
  UnknownError = "DICTIONARY.UNKNOWN_ERROR",
}
