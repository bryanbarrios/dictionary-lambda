export type SerializedException = {
  name: string;
  message: string;
  code: string;
  stack?: string;
  cause?: string;
  metadata?: unknown;
};

export type SerializedHTTPException = SerializedException & {
  status: number;
};

export enum GenericExceptionCode {
  ArgumentInvalid = "GENERIC.ARGUMENT_INVALID",
  ArgumentOutOfRange = "GENERIC.ARGUMENT_OUT_OF_RANGE",
  ArgumentNotProvided = "GENERIC.ARGUMENT_NOT_PROVIDED",
  NotFound = "GENERIC.NOT_FOUND",
  Conflict = "GENERIC.CONFLICT",
  InternalServerError = "GENERIC.INTERNAL_SERVER_ERROR",
  UnrecoverableError = "GENERIC.UNRECOVERABLE_ERROR",
}
