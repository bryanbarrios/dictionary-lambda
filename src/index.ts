import {
  searchEntry,
  type SearchEntryVariables,
} from "./api/dictionary/search-word";
import type { HTTPException } from "./lib/exceptions/exceptions";
import type { Entries } from "./types/dictionary";
import type { Handler } from "./types/handler";

/**
 * AWS Lambda function handler for searching a dictionary entry.
 *
 * @param event - The event object containing the search entry variable.
 * @returns A Promise that resolves to the search results.
 * @throws {HTTPException} If an error occurs during the search.
 */
export const handler: Handler<
  SearchEntryVariables,
  Entries,
  HTTPException
> = async (event) => {
  const entry = event.variables.entry;

  return await searchEntry({ entry });
};
