import axios from "axios";

import { ENDPOINTS } from "@/lib/constants/endpoints";
import { HTTPException } from "@/lib/exceptions/exceptions";
import { client } from "@/lib/http/client";
import { Entries, DictionaryExceptionCode } from "@/types/dictionary";
import { StatusCode } from "@/types/http";
import { GenericExceptionCode } from "@/types/exception";

export type SearchEntryVariables = {
  entry: string;
};

/**
 * Searches for a dictionary entry.
 * @param entry - The word or phrase to search for.
 * @returns A Promise that resolves to the search results.
 * @throws {HTTPException} If an error occurs during the search.
 */
export const searchEntry = async ({
  entry,
}: SearchEntryVariables): Promise<Entries> => {
  try {
    const endpoint = `${ENDPOINTS.dictionary.entries}/${entry}`;

    return await client.get(endpoint);
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const response = error?.response;

      if (response) {
        const statusCode = response.status;

        switch (statusCode) {
          case StatusCode.NotFound:
            throw new HTTPException(
              DictionaryExceptionCode.WordNotFound,
              StatusCode.NotFound,
              `${entry} entry not found.`,
              error
            );
          default:
            throw new HTTPException(
              DictionaryExceptionCode.UnknownError,
              StatusCode.InternalServerError,
              `An error occurred while searching for entry ${entry}.`,
              error
            );
        }
      }
    }

    throw new HTTPException(
      GenericExceptionCode.InternalServerError,
      StatusCode.InternalServerError,
      "Something went wrong. Help us improve your experience by sending an error report.",
      error
    );
  }
};
