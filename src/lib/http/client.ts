import axios from "axios";

import { appConfig } from "@/config/app";

export const client = axios.create({
  baseURL: new URL(appConfig.api.apiVersion, appConfig.api.apiURL).href,
});

client.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    const message = error.response?.data?.message || error.message;

    return Promise.reject(error);
  }
);
