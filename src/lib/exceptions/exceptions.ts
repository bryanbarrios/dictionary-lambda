import type { StatusCode } from "@/types/http";
import type { SerializedHTTPException } from "@/types/exception";
import { ExceptionBase } from "./exception-base";

/**
 * Exception class for HTTP-related errors.
 * Extends the `ExceptionBase` class to provide additional HTTP-specific properties.
 */
export class HTTPException extends ExceptionBase {
  /**
   * The unique code for this HTTP exception.
   */
  readonly code: string;

  /**
   * The HTTP status code associated with this exception.
   */
  readonly status: StatusCode;

  static readonly name = "HTTPException";

  /**
   * Constructs a new instance of the `HTTPException` class.
   * @param code - The unique code for this HTTP exception.
   * @param status - The HTTP status code associated with this exception.
   * @param message - The error message for the exception.
   * @param cause - The cause of the exception, if any.
   * @param metadata - Additional metadata associated with the exception.
   */
  constructor(
    code: string,
    status: StatusCode,
    message: string,
    cause?: unknown,
    metadata?: unknown
  ) {
    super(message, cause, metadata);
    this.code = code;
    this.status = status;
  }

  /**
   * Serializes the HTTP exception to a JSON-compatible object.
   * @returns A `SerializedHTTPException` object containing the exception details.
   */
  toJSON(): SerializedHTTPException {
    return {
      ...super.toJSON(),
      status: this.status,
    };
  }
}
