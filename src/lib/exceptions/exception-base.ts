import type { SerializedException } from "@/types/exception";

/**
 * Abstract base class for custom exceptions.
 * Provides common functionality and properties for exception handling.
 */
export abstract class ExceptionBase extends Error {
  /**
   * The unique code for this exception.
   * Subclasses must implement this property.
   */
  abstract readonly code: string;

  /**
   * The name of the exception class.
   * Set dynamically based on the subclass.
   */
  readonly name: string;

  /**
   * The cause of the exception, if any.
   * Can be any type of object.
   */
  readonly cause?: unknown;

  /**
   * Additional metadata associated with the exception.
   * Can be any type of object.
   */
  readonly metadata?: unknown;

  /**
   * Constructs a new instance of the `ExceptionBase` class.
   * @param message - The error message for the exception.
   * @param cause - The cause of the exception, if any.
   * @param metadata - Additional metadata associated with the exception.
   */
  constructor(message: string, cause?: unknown, metadata?: unknown) {
    super(message);
    this.name = this.constructor.name;
    this.cause = cause;
    this.metadata = metadata;
    Error.captureStackTrace(this, this.constructor);
  }

  /**
   * Serializes the exception to a JSON-compatible object.
   * @returns A `SerializedException` object containing the exception details.
   */
  toJSON(): SerializedException {
    return {
      name: this.name,
      message: this.message,
      code: this.code,
      stack: this.stack,
      cause: this.cause ? JSON.stringify(this.cause) : undefined,
      metadata: this.metadata,
    };
  }
}
