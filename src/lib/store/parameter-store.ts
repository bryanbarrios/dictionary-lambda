import { GetParameterCommand } from "@aws-sdk/client-ssm";

import { ssmClient } from "./client";

type GetParameterValueParams = {
  name: string;
  withDecryption?: boolean;
};

export const getParameterValue = async (args: GetParameterValueParams) => {
  const input = {
    Name: args.name,
    WithDecryption: args.withDecryption,
  };

  const command = new GetParameterCommand(input);

  const response = await ssmClient.send(command);

  return response?.Parameter?.Value;
};
