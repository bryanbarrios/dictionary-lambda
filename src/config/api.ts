import dotenv from "dotenv";

import type { APIConfig } from "@/types/config";

dotenv.config();

export const apiConfig = {
  apiURL: process.env.API_URL || "",
  apiVersion: process.env.API_VERSION || "",
} satisfies APIConfig;
