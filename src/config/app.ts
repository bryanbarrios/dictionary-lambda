import type { AppConfig } from "@/types/config";
import { apiConfig } from "./api";

export const appConfig = {
  api: apiConfig,
} satisfies AppConfig;
