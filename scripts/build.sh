#!/bin/bash

# Step 1 - Build Lambda Function
echo "Building the Lambda..."
pnpm tsup

# Step 2 - Remove development dependencies
echo "Removing development dependencies..."
pnpm i --prod

# Step 3 - Copy node_modules into dist/
echo "Copying node_modules into dist/..."
cp -rf node_modules dist/

# Step 4 - Compress dist/ and node_modules into index.zip
echo "Compressing dist/ content and node_modules into index.zip..."
cd dist
bestzip ../index.zip *
rm -rf node_modules
cd ..

# Step 5 - Reinstall all dependencies
echo "Reinstalling all dependencies..."
pnpm i

echo "Build completed."
