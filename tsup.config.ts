import { defineConfig } from "tsup";

const config = defineConfig({
  entry: ["src"],
  format: ["cjs"],
  splitting: false,
  minify: true,
  clean: true,
});

export default config;
